﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestAppSignalR.AppService.DomainModels
{
    public class JwtToken
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}
