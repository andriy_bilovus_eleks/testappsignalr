﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestAppSignalR.AppService.DomainModels;

namespace TestAppSignalR.AppService.Services.Declarations
{
    public interface ITokenProvider
    {
        Task<JwtToken> GenerateToken(string email);
    }
}
