﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestAppSignalR.AppService.DomainModels;

namespace TestAppSignalR.AppService.Services.Declarations
{
    public interface IUserService
    {
        Task<JwtToken> ValidateLogin(string email, string password);
    }
}
