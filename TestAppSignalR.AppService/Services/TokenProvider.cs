﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestAppSignalR.AppService.DomainModels;
using TestAppSignalR.AppService.Services.Declarations;

namespace TestAppSignalR.AppService.Services
{
    public class TokenProvider : ITokenProvider
    {
        public Task<JwtToken> GenerateToken(string email)
        {
            return Task.FromResult(new JwtToken
            {
                AccessToken = "QpwL5tke4Pnpja7X4",
                RefreshToken = "refresh_token"
            });
        }
    }
}
