﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestAppSignalR.AppService.DomainModels;
using TestAppSignalR.AppService.Exceptions;
using TestAppSignalR.AppService.Services.Declarations;

namespace TestAppSignalR.AppService.Services
{
    public class UserService : IUserService
    {
        private readonly ITokenProvider _tokenProvider;

        public UserService(ITokenProvider tokenProvider)
        {
            _tokenProvider = tokenProvider;
        }

        public async Task<JwtToken> ValidateLogin(string email, string password)
        {
            
            // repository.CheckIfUserExists(email).


            // Replace with call to database and check if hash of password match.
            if (password.ToUpper() == "123456")
            {
                return await _tokenProvider.GenerateToken(email);
            }

            throw new InvalidPasswordException();
        }
    }
}
