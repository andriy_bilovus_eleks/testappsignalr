import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignalrService {

  private hub: signalR.HubConnection;

  loginError$ = new Subject<string>();

  constructor() { }

  public initHub() {
    this.hub = new signalR.HubConnectionBuilder()
      .withUrl('/api/messageHub')
      .build();

    this.hub.start().then(() => {
      console.log('Message Hub connected!');

      this.hub.on('LoginFailed', (message) => {
        console.log(message);
        this.loginError$.next(message);
      });
    });
  }
}
