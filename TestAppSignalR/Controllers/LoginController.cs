﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestAppSignalR.AppService.Exceptions;
using TestAppSignalR.AppService.Services.Declarations;
using TestAppSignalR.Hubs;
using TestAppSignalR.Hubs.Declarations;
using TestAppSignalR.ViewModels;

namespace TestAppSignalR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IHubContext<MessageHub> _hubContext;

        public LoginController(IUserService userService, IHubContext<MessageHub> hubContext)
        {
            _userService = userService;
            _hubContext = hubContext;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody]LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                return Ok(await _userService.ValidateLogin(model.Email, model.Password));
            }
            catch (InvalidPasswordException)
            {
                await _hubContext.Clients.All.SendAsync("LoginFailed", $"{model.Email} login failed at {DateTime.Now}");
                return BadRequest("Invalid password or/and email");
            }
        }
    }
}