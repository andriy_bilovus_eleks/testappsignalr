﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestAppSignalR.Hubs.Declarations;

namespace TestAppSignalR.Hubs
{
    public class MessageHub : Hub
    {
        public async Task SendMessage(string email)
        {
            await Clients.Caller.SendAsync("LoginFailed", $"{email} login failed at {DateTime.Now}");
        }
    }
}
